#!/bin/bash

# Adapted from Stanley Lewis' tutorial (https://www.youtube.com/watch?v=4W2YY-qBla0)

# remove all non-running containers
docker rm -f $(docker ps -a -q)

# remove all untagged images
docker rmi -f $(docker images | grep "<none>" | awk '{print $3}')

docker rmi -f iew_activemq
docker rmi -f iew_apache
docker rmi -f iew_portal

