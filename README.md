# README #

Configuration for the IEW server.

### Building ###

// reset  
./docker-clean.sh  
// build  
docker-compose build  
// first start up with initialize db  
docker-compose -f docker-compose.yml -p iew --x-networking up  
// shutdown  
docker-compose stop  

### Running ###

docker-compose --x-networking -p iew up --no-recreate  

### Misc. ###
* "Couldn't connect to Docker daemon" on Mac and Cygwin --> eval "$(docker-machine env default)"
